#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty

def before_shutdown():
    # disable dbw when close this node
    pub_disable.publish(empty)


if __name__=='__main__':
    rospy.init_node('dataspeed_enabler')
    pub_enable = rospy.Publisher("/vehicle/enable", Empty, queue_size=5)
    pub_disable = rospy.Publisher("/vehicle/disable", Empty, queue_size=5)
    rospy.loginfo("dataspeed_enabler was started") 

    rospy.on_shutdown(before_shutdown)
    
    rate = rospy.Rate(100)

   
    while pub_enable.get_num_connections() == 0:
        rate.sleep()


    empty = Empty()
    # enable dbw
    pub_enable.publish(empty)

    while not rospy.is_shutdown():
    
        rate.sleep()

    rospy.loginfo("dataspeed_enabler was stopped")    
