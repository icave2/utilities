#!/usr/bin/env python

#
#  Title:        gps to KML
#  Description:  ROS node to write KML file from gps messages.
#
# Adapted from: https://github.com/hitzg/bag_tools/blob/master/bag_to_kml.py
# Adapted from: https://github.com/ethz-asl/ethz_piksi_ros/tree/master/piksi_rtk_kml

import rospy
import time

from std_msgs.msg import String
from gps_common.msg import GPSFix
from datetime import datetime









def callback_receive_gpsfix(fix):
    global time_last_writing
    global waypoint_counter
    global placemarker_prefix_name



    if rospy.get_time() >= (time_last_writing + sampling_period):

        waypoint_counter = waypoint_counter + 1
        waypoint_name = ""  #placemarker_prefix_name + str(waypoint_counter)
        utc_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(fix.header.stamp.to_sec()))
        lat = fix.latitude
        lon = fix.longitude
        alt = fix.altitude
        description = 'gps point'
        kml_string = ""
        kml_string = kml_placemark(waypoint_name, utc_time, lat, lon, alt, description)

        file_obj.write(kml_string)
        time_last_writing = rospy.get_time()

def close_kml_file_handler():
    file_obj.write(kml_tail())
    file_obj.close()
    rospy.loginfo(rospy.get_name() + ' KML file written in \'%s\'' % kml_file_path)

    # Adapted from: https://github.com/hitzg/bag_tools/blob/master/bag_to_kml.py
def kml_head(name):
    return '''<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<Document>
<name>%s</name>
''' % (name)

    # Adapted from: https://github.com/hitzg/bag_tools/blob/master/bag_to_kml.py
def kml_tail():
    return '''</Document>
</kml>'''

def kml_placemark(name, timestamp, lat, lon, alt=0, description=''):
    return '''
<Placemark>
    <name>%s</name>
    <TimeStamp>
        <when>%s</when>
    </TimeStamp>
    <Point>
        <coordinates>%f,%f,%f</coordinates>
    </Point>
    <description>%s</description>
</Placemark>
''' % (name, timestamp, lon, lat, alt, description)  # KML wants first lon and then lat.

 



if __name__=='__main__':
    rospy.init_node('gps2klm')
    rospy.loginfo("gps2klm node was started")




    # KML file
    save_folder_path = "/home/us1/Documents"
    kml_file_name = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    kml_file_path = "%s/kml/%s.kml" % (save_folder_path, kml_file_name)
    file_obj = open(kml_file_path, 'w')
    file_obj.write(kml_head(kml_file_name))

    # Settings.
    sampling_period = 1.0 / 5.0
    extrude_point = False
    placemarker_prefix_name = "WP"
    tessellate = 0
    kml_altitude_mode = "absolute"

    # Variables.
    waypoint_counter = 0
    time_last_writing = rospy.get_time()



    rows_count=0
    
    sub = rospy.Subscriber("/gps",GPSFix,callback_receive_gpsfix)


    rospy.on_shutdown(close_kml_file_handler)
    rospy.spin()

    

    rospy.loginfo("gps2kml node was stopped")