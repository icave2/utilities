#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from gps_common.msg import GPSFix
import csv


n=0
rows_count=0
all_rows = []

def callback_receive_gpsfix(fix):
    global n
    global rows_count
    global all_rows
    n+=1
    rows_count+=1
    if rows_count<63000:
        if n > 3:
            n=0
            fix_row=[fix.header.stamp.secs, fix.latitude, fix.longitude, fix.track, fix.speed]
            all_rows.append(fix_row)
            rospy.loginfo(fix_row)

        #rospy.loginfo("Message GPSFix received:")
        
        #rospy.loginfo(fix.header.stamp.secs)
        #rospy.loginfo(fix_row)
        #rospy.loginfo("lat=%f, long=%f, heading=%f, speed=%d" % (fix.latitude, fix.longitude, fix.track, fix.speed))

def write_csv_file (file_name, _all_rows):
    f = open(file_name, 'w')

    with f:
        fnames = ['time', 'lattitude', 'longitude', 'track', 'speed']
        writer = csv.writer(f)
        writer.writerow(fnames)
        writer.writerows(_all_rows)
    rospy.loginfo("*")
    rospy.loginfo("*")
    rospy.loginfo("GPS data was saved to file: %s" % file_name)



if __name__=='__main__':
    rospy.init_node('gps2csv')
    rospy.loginfo("gps2csv node was started")
    rows_count=0
    
    sub = rospy.Subscriber("/gps",GPSFix,callback_receive_gpsfix)

    rospy.spin()

    write_csv_file('test.csv',all_rows)

    rospy.loginfo("gps2csv node was stopped")